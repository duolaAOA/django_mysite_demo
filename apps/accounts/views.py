# -*-coding:utf-8 -*-

from django.shortcuts import render, redirect
from django.contrib.auth import login as auth_login
from django.utils.decorators import method_decorator
from django.views.generic import UpdateView
from django.contrib.auth.models import User
from django.urls import reverse_lazy
from django.contrib.auth.decorators import login_required

from accounts.forms import SignUpForm
# Create your views here.


def signup(request):
    """
    注册
    http://www.jb51.net/article/87046.htm
    """
    if request.method == 'POST':
        register_form = SignUpForm(request.POST)
        if register_form.is_valid():
            user = register_form.save()
            auth_login(request, user)
            return redirect('home')
    else:
        register_form = SignUpForm()
    return render(request, 'signup.html', {'form': register_form})


@method_decorator(login_required, name="dispatch")
class UserUpdateView(UpdateView):
    model = User
    fields = ['first_name', 'last_name', 'email',]
    template_name = "my_account.html"
    success_url = reverse_lazy("my_account")

    def get_object(self):
        return self.request.user