# -*-coding:utf-8 -*-

# -*-coding:utf-8 -*-
from django.db.models import Count
from django.shortcuts import render, get_object_or_404, redirect
from django.core.paginator import Paginator, EmptyPage,PageNotAnInteger
from django.contrib.auth.decorators import login_required
from django.views.generic import CreateView, UpdateView, ListView
from django.utils import timezone
from django.utils.decorators import method_decorator
from django.core.urlresolvers import reverse

from .models import Board, Topic, Post
from .forms import NewTopicForm, PostForm
# Create your views here.


class BoardListView(ListView):
    model = Board
    context_object_name = 'boards'
    template_name = 'home.html'


class TopicListView(ListView):
    model = Topic
    context_object_name = "topics"
    template_name = "topics.html"
    paginate_by = 20

    def get_context_data(self, **kwargs):
        kwargs["board"] = self.board
        return super(TopicListView, self).get_context_data(**kwargs)

    def get_queryset(self):
        self.board = get_object_or_404(Board, pk=self.kwargs.get('pk'))
        queryset = self.board.topics.order_by('-last_updated').annotate(replies=Count('posts') - 1)
        return queryset


@login_required    # 对为登录用户重定向
def new_topic(request, pk):
    board = get_object_or_404(Board, pk=pk)
    if request.method == 'POST':
        form = NewTopicForm(request.POST)       # 表单验证
        if form.is_valid():
            topic = form.save(commit=False)
            topic.board = board
            topic.starter = request.user   # 当前用户
            topic.save()
            Post.objects.create(
                message=form.cleaned_data.get('message'),
                topic=topic,
                created_by=request.user
            )
            return redirect('topic_posts', pk=board.pk, topic_pk=topic.pk)
    else:
        form = NewTopicForm()   # 错误添加到表单，声明返回
    return render(request, 'new_topic.html', {"board": board, "form": form})


class PostListView(ListView):
    """帖子列表"""
    model = Post
    context_object_name = "posts"
    template_name = "topic_posts.html"
    paginate_by = 20

    def get_context_data(self, **kwargs):

        session_key = "view_topic_{}".format(self.topic.pk)
        if not self.request.session.get(session_key, False):
            self.topic.views += 1
            self.topic.save()
            self.request.session[session_key] = True

        kwargs["topic"] = self.topic
        return super(PostListView, self).get_context_data(**kwargs)

    def get_queryset(self):
        self.topic = get_object_or_404(Topic, board__pk=self.kwargs.get('pk'), pk=self.kwargs.get('topic_pk'))
        queryset = self.topic.posts.order_by('created_at')
        return queryset


@login_required
def reply_topic(request, pk, topic_pk):
    """帖子回复"""
    topic = get_object_or_404(Topic, board__pk=pk, pk=topic_pk)
    if request.method == 'POST':
        form = PostForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            post.topic = topic
            post.created_by = request.user
            topic.last_updated = timezone.now()   # 更新
            post.save()

            topic.last_updated = timezone.now()
            topic.save()

            topic_url = reverse('topic_posts', kwargs={'pk': pk, 'topic_pk': topic_pk})
            topic_post_url = '{url}?page={page}#{id}'.format(
                url=topic_url,
                id=post.pk,
                page=topic.get_page_count()
            )
            return redirect(topic_post_url)

    else:
        form = PostForm()
    return render(request, 'reply_topic.html', {'topic': topic, 'form': form})


@method_decorator(login_required, name='dispatch')
class PostUpdateView(UpdateView):
    """
    不能用@login_required装饰器直接装饰类。我们必须使用该实用程序 @method_decorator，
    并传递一个装饰器（或一个装饰器列表），并告诉应该装饰哪个方法。在基于类的视图中，
    装饰调度方法是很常见的。这是一个Django使用的内部方法（在View类中定义）
    """
    model = Post
    fields = ('message', )
    template_name = 'edit_post.html'
    pk_url_kwarg = 'post_pk'
    context_object_name = 'post'

    def get_queryset(self):
        queryset = super(PostUpdateView, self).get_queryset()
        return queryset.filter(created_by=self.request.user)

    def form_valid(self, form):
        post = form.save(commit=False)
        post.updated_by = self.request.user
        post.updated_at = timezone.now()
        post.save()

        return redirect('topic_posts', pk=post.topic.board.pk, topic_pk=post.topic.pk)


