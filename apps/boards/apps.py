from django.apps import AppConfig


class BoardsConfig(AppConfig):
    name = 'boards'
    verbose_name = "面板"