# -*-coding:utf-8 -*-

from django import forms
from .models import Topic, Post


class NewTopicForm(forms.ModelForm):
    """
    新话题字段
    """
    message = forms.CharField(widget=forms.Textarea(
                              attrs={'rows': 5, 'placeholder': '你在想什么?'}),
                              max_length=4000,
                              help_text='最大长度4000', label="正文")
    # 用来渲染成HTML元素的工具.forms.Textarea对应HTML中的<textarea>标签

    class Meta:
        model = Topic
        fields = ['subject', 'message']


class PostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields = ['message', ]