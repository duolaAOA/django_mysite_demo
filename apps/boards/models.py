# -*-coding:utf-8-*-
from django.db import models
from django.contrib.auth.models import User
from django.utils.text import Truncator
from django.utils.html import mark_safe

import math

from markdown import markdown
# Create your models here.


class Board(models.Model):
    name = models.CharField(max_length=30, unique=True, verbose_name="姓名")
    description = models.CharField(max_length=100, verbose_name="描述")

    class Meta:
        verbose_name = "面板"
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.name

    def get_posts_count(self):
        return Post.objects.filter(topic__board=self).count()

    def get_last_post(self):
        return Post.objects.filter(topic__board=self).order_by('-created_at').first()


class Topic(models.Model):
    subject = models.CharField(max_length=255, verbose_name="主题")
    last_updated = models.DateTimeField(auto_now_add=True, verbose_name="最近更新")
    board = models.ForeignKey(Board, related_name='topics', verbose_name="话题页")
    starter = models.ForeignKey(User, related_name='topics', verbose_name="创建者")
    views = models.PositiveIntegerField(default=0, verbose_name="浏览次数")

    class Meta:
        verbose_name = "主题"
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.subject

    def get_page_count(self):
        count = self.posts.count()
        pages = count / 20
        return math.ceil(pages)

    def has_many_pages(self, count=None):
        if count is None:
            count = self.get_page_count()
        return count > 6

    def get_page_range(self):
        count = self.get_page_count()
        if self.has_many_pages():
            return range(1, 5)
        return range(1, int(count) + 1)

    def get_last_len_posts(self):
        """限制最近10个帖子"""
        return self.posts.order_by('-created_at')[:10]

class Post(models.Model):
    message = models.TextField(max_length=4000, verbose_name="正文内容")
    topic = models.ForeignKey(Topic, related_name='posts', verbose_name="话题")
    created_at = models.DateTimeField(auto_now_add=True, verbose_name="创建时间")
    updated_at = models.DateTimeField(null=True, verbose_name="更新时间")
    created_by = models.ForeignKey(User, related_name='posts', verbose_name="创建者")
    updated_by = models.ForeignKey(User, null=True, related_name='+', verbose_name="更新者")     # 忽略反向关系

    class Meta:
        verbose_name = "帖子详情"
        verbose_name_plural = verbose_name

    def __str__(self):
        # 文本截断 https://docs.djangoproject.com/en/2.0/_modules/django/utils/text/
        truncated_message = Truncator(self.message)
        return truncated_message.chars(30)

    def get_message_as_markdown(self):
        """{{ post.message }}  to {{ post.get_message_as_markdown }}
        处理用户输入
        simpleMD  markdown编辑器
        """
        return mark_safe(markdown(self.message, safe_mode='escape'))