# -*-coding:utf-8 -*-

import hashlib
from urllib.parse import urlencode

from django import template
from django.conf import settings

register = template.Library()
"""个人资料"""

@register.filter
def gravatar(user):
    # https://fi.gravatar.com/site/implement/images/python/
    email = user.email.lower().encode('utf-8')
    default = 'https://en.gravatar.com/userimage/134713533/5fcc488a81c195ce4288d84732564af3.jpeg'
    size = 256
    url = 'https://www.gravatar.com/avatar/{md5}?{params}'.format(
        md5=hashlib.md5(email).hexdigest(),
        params=urlencode({'d': default, 's': str(size)})
    )
    return url