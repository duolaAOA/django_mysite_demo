"""mysite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.contrib.auth import views as auth_views

from accounts import views as accounts_views
from boards.views import BoardListView, TopicListView, PostListView, PostUpdateView
from boards import views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', BoardListView.as_view(), name='home'),    # 首页
    url(r'^signup/$', accounts_views.signup, name='signup'),    # 注册
    url(r'^login/$', auth_views.LoginView.as_view(template_name='login.html'), name='login'),   # 登录
    url(r'^logout/$', auth_views.LogoutView.as_view(), name='logout'),   # 退出登录
    url(r'^boards/(?P<pk>\d+)/$', TopicListView.as_view(), name='board_topics'),     # 话题
    url(r'^boards/(?P<pk>\d+)/new/$', views.new_topic, name='new_topic'),   # 添加新话题
    url(r'^boards/(?P<pk>\d+)/topics/(?P<topic_pk>\d+)/$', PostListView.as_view(), name='topic_posts'),   # 帖子列表页
    url(r'^boards/(?P<pk>\d+)/topics/(?P<topic_pk>\d+)/reply/$', views.reply_topic, name='reply_topic'),  # 回帖
    # 编辑
    url(r'^boards/(?P<pk>\d+)/topics/(?P<topic_pk>\d+)/posts/(?P<post_pk>\d+)/edit/$',
        PostUpdateView.as_view(), name='edit_post'),

    # 密码修改
    url(r'^reset/$', auth_views.PasswordResetView.as_view(
                                template_name='password_reset.html',
                                email_template_name='password_reset_email.html',
                                subject_template_name='password_reset_subject.txt'),
                                name='password_reset'),    # (重置密码) 邮件正文与主题

    url(r'^reset/done/$', auth_views.PasswordResetDoneView.as_view(
                                template_name='password_reset_done.html'), name='password_reset_done'),

    # 忘记密码
    url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        auth_views.PasswordResetConfirmView.as_view(
                                template_name='password_reset_confirm.html'), name='password_reset_confirm'),

    # 个人中心
    url(r'^settings/account/$', accounts_views.UserUpdateView.as_view(), name='my_account'),

    # 密码修改完成
    url(r'^reset/complete/$', auth_views.PasswordResetCompleteView.as_view(
                                template_name='password_reset_complete.html'), name='password_reset_complete'),
    # 修改密码
    url(r'^settings/password/$', auth_views.PasswordChangeView.as_view(
                                template_name='password_change.html'), name='password_change'),

    # 密码修改完成
    url(r'^settings/password/done/$', auth_views.PasswordChangeDoneView.as_view(
                                template_name='password_change_done.html'), name='password_change_done'),


]
